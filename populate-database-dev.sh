#!/bin/sh
#wget http://www.lexique.org/databases/Lexique383/Lexique383.tsv

read -p "Are you using podman ? [Y/n] : " podman

if [ "$podman" = 'Y' ]; then
    podman cp Lexique383.tsv orthogram_db_dev:/
    podman cp table-creation.sql orthogram_db_dev:/
    podman exec -it orthogram_db_dev bash -c "cat table-creation.sql | mysql -uorthogram -p orthogram"
else
    docker cp Lexique383.tsv orthogram_db_dev:/
    docker cp table-creation.sql orthogram_db_dev:/
    docker exec -it orthogram_db_dev bash -c "cat table-creation.sql | mysql -uorthogram -p orthogram"
fi
