const configure = () => {
    let grammaticalCategory = document.getElementById('grammatical-categories');
    let minFrequency = document.getElementById('min-frequency');
    let maxFrequency = document.getElementById('max-frequency');
    let nbWords = document.getElementById('nb-words');
    const submitButton = document.getElementById('submit-config');

    minFrequency.addEventListener('change', (event) => {
        checkFrequency();
    })
    maxFrequency.addEventListener('change', (event) => {
        checkFrequency();
    })

    const checkFrequency = () => {
        if (parseFloat(minFrequency.value) > parseFloat(maxFrequency.value)) {
            submitButton.setAttribute('disabled', true);
            return false;
        }
        if (submitButton.hasAttribute('disabled')) {
            submitButton.removeAttribute('disabled');
        }
        return true;
    };

    submitButton.addEventListener('click', (event) => {
        event.preventDefault();
        pushConfigState(
            grammaticalCategory.value,
            minFrequency.value,
            maxFrequency.value,
            nbWords.value
        );
        loadContent();
    });
};

const pushConfigState = (grammar, minFrequency, maxFrequency, nb) => {
    let state = {
        grammar: grammar,
        minFrequency: minFrequency,
        maxFrequency: maxFrequency,
        nb: nb
    };
    let title = "config";
    let url = "?grammar=" + grammar +
    "&minFrequency=" + minFrequency +
    "&maxFrequency=" + maxFrequency +
    "&nb=" + nb
    history.pushState(state, title, url);
};
