window.addEventListener('load', (event) => {
    let nbSearchedWords;
    showConfig();
    loadState();
    loadContent();
    validateWords();
    restart();

    configure();
});

const loadState = () => {
    const parsedUrl = new URL(window.location.href);
    let grammar = parsedUrl.searchParams.get("grammar");
    let minFrequency = parsedUrl.searchParams.get("minFrequency");
    let maxFrequency = parsedUrl.searchParams.get("maxFrequency");
    let nb = parsedUrl.searchParams.get("nb");
    if (grammar && minFrequency && maxFrequency && nb) {
        pushConfigState(grammar, minFrequency, maxFrequency, nb);
    } else {
        pushConfigState('ADJ', '0', '38928.9', '48'); // Default config
    }
};
