const loadContent = () => {
    loadWords();
    loadInstruction();
};

const removeWords = () => {
    let wordsContainer = document.getElementById('words-container');
    while (wordsContainer.firstChild) {
        wordsContainer.removeChild(wordsContainer.firstChild);
    }
};

const loadWords = () => {
    removeWords();
    url = new URL(window.location.origin + "/api/getRandomWords.php");
    let params = {
        minFrequency: history.state.minFrequency,
        maxFrequency: history.state.maxFrequency,
        nb: history.state.nb,
        searchedGrammar: history.state.grammar,
    }
    url.search = new URLSearchParams(params).toString();

    fetch(url)
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            throw new Error(response.status + ' ' + response.statusText);
        })
        .then(data => {
            nbSearchedWords = data.nbWordsSearched;
            buildWords(data.wordsArray);
            checkWord();
        })
        .catch((error) => {
            console.error(error);
            alert(error);
        });
};

const buildWords = (wordsArray) => {
    let wordsContainer = document.getElementById('words-container');

    for(let word of wordsArray) {
        let wordDiv = document.createElement('div');
        let wordP = document.createElement('p');

        wordsContainer.appendChild(wordDiv);
        wordDiv.classList.add('word','undefined')
        wordDiv.appendChild(wordP);
        wordP.textContent = word;
    }
};

const loadInstruction = () => {
    const categories = {
        'ADJ': 'adjectif',
        'ADV': 'adverbe',
        'ART': 'article',
        'AUX': 'auxiliaire',
        'CON': 'conjonction',
        'LIA': 'liaison euphonique',
        'NOM': 'nom',
        'ONO': 'onomatopée',
        'PRE': 'préposition',
        'PRO': 'pronom',
        'VER': 'verbe',
    }
    let instruction = document.getElementById('instruction');
    let searchedCategory = history.state.grammar;

    instruction.textContent = 'Cliquez sur les ' + categories[searchedCategory] + 's';
}
