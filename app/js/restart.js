const restart = () => {
    let result = document.getElementById('result');
    let restartButton = document.getElementsByClassName('restart-button')[0];
    let resultInstruction = document.getElementsByClassName('result-instruction')[0];
    let validateButton = document.getElementsByClassName('validate-words')[0];

    restartButton.addEventListener('click', (event) => {
        restartButton.classList.remove('show');
        resultInstruction.classList.remove('show');

        validateButton.classList.add('show');

        loadContent();
    })
}
