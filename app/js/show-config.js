const showConfig = () => {
    let config = document.getElementsByClassName('config');
    let configPanel = config[0];

    const configButton = document.getElementById('config-button');
    configButton.addEventListener('click', (event) => {
        event.preventDefault();
        event.stopPropagation();
        configPanel.classList.toggle('show');
    });

    configPanel.addEventListener('click', (event) => {
        event.stopPropagation();
    });

    //hides the section when we click somewhere else
    window.addEventListener('click', (event) => {
        if (configPanel.classList.contains('show')) {
            configPanel.classList.toggle('show');
        }
    })

};
