const checkWord = () => {
    let words = document.getElementsByClassName('undefined')
    Array.from(words).forEach((word) => {
        word.addEventListener('click', (event) => {
            if (word.classList.contains("undefined")) {
                searchWord(word);
            }
        })
    });
}

const searchWord = (word) => {
    let textWord = word.getElementsByTagName('p')[0].textContent;

    url = new URL(window.location.origin + "/api/searchWord.php");
    let params = {
        word: textWord,
        grammar: history.state.grammar,
    }
    url.search = new URLSearchParams(params).toString();

    fetch(url)
        .then((response) => {
            if (response.ok) {
                return response.json();
            }
            throw new Error(response.status + ' ' + response.statusText);
        })
        .then((data) => {
            if (data.exists) {
                setGoodWord(word);
                return true;
            }
            setBadWord(word);
        })
        .catch((error) => {
            console.error(error);
            alert(error);
        });
}

const setGoodWord = (word) => {
    nbSearchedWords--;
    const tick = document.createElement('div');
    word.appendChild(tick);
    tick.classList.add('tick');

    word.classList.replace('undefined', history.state.grammar);
}

const setBadWord = (word) => {
    const cross = document.createElement('div');
    word.appendChild(cross);
    cross.classList.add('cross');
}
