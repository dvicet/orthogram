const validateWords = () => {
    let validateButton = document.getElementsByClassName('validate-words')[0];
    let result = document.getElementById('result');
    let resultInstruction = document.getElementsByClassName('result-instruction')[0];
    let restartButton = document.getElementsByClassName('restart-button')[0];

    validateButton.addEventListener('click', (event) => {

        if (nbSearchedWords === 0) {
            validateButton.classList.remove('show');

            resultInstruction.classList.add('show')
            resultInstruction.textContent = "Bravo, vous avez trouvé tous les mots !";

            restartButton.classList.add('show');
        } else {
            resultInstruction.classList.add('show')
            resultInstruction.textContent = "il reste encore " + nbSearchedWords + " mots à trouver";
        }
    });
};
