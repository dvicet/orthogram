<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require "config/autoload.php";
require "config/database.php";

const MAX_FREQUENCY = 38928.9;
const MAX_LIMIT = 1000;

$database = new Database();
$db = $database->getConnection();
$wordManager = new WordsManager($db);

$resultArray = getRandomWords($wordManager);
$result = false;
switch ($resultArray) {
    case '1': //minFrequency out
        $message = "minFrequency should be between 0 and ".MAX_FREQUENCY;
        break;
    case '2': //maxFrequency out
        $message = "maxFrequency should be between 0 and ".MAX_FREQUENCY;
        break;
    case '3': //maxFrequency < minFrequency
        $message = "maxFrequency should be higher than minFrequency";
        break;
    case '4': //nbWords out
        $message = "nb should be between 1 and ".MAX_LIMIT;
        break;
    case '5': //minFrequency not float
        $message = "minFrequency should be a float number";
        break;
    case '6':
        $message = "searchedGrammar is not valid";
        break;

    default: // ok
        $result = true;
        break;
}

if ($result) {
    $words = $resultArray[0];
    $searchedGrammar = $resultArray[1];
    $arrayWords = [];
    $nbWordsSearched = 0;
    foreach ($words as $word) {
        $arrayWords[] = $word->word();
        if (in_array($searchedGrammar, $word->possibleGrammaticalCategory())) {
            $nbWordsSearched++;
        }
    }
    echo json_encode([
            "result" => $result,
            "wordsArray" => $arrayWords,
            "nbWordsSearched" => $nbWordsSearched,
        ]);
} else {
    http_response_code(400); // Bad Request
    echo json_encode([
            "result" => $result,
            "message" => $message,
        ]);
}


function getRandomWords(WordsManager $manager) {
    if (isset($_GET['minFrequency'])) {
        /*if(!is_float(floatval($_GET['minFrequency']))) {
            return 5;
        }*/
        if ($_GET['minFrequency'] < 0 OR $_GET['minFrequency'] > MAX_FREQUENCY) {
            return 1;
        }
        $minFrequency = $_GET['minFrequency'];
    } else {
        $minFrequency = 0;
    }

    if (isset($_GET['maxFrequency'])) {
        if ($_GET['maxFrequency'] < 0 OR $_GET['maxFrequency'] > MAX_FREQUENCY) {
            return 2;
        }
        if ($_GET['maxFrequency'] < $_GET['minFrequency']) {
            return 3;
        }
        $maxFrequency = $_GET['maxFrequency'];
    } else {
        $maxFrequency = MAX_FREQUENCY;
    }

    if (isset($_GET['nb'])) {
        if ($_GET['nb'] < 1 OR $_GET['nb'] > MAX_LIMIT) {
            return 4;
        }
        $nbWords = intval($_GET['nb']);
    } else {
        $nbWords = 50;
    }

    if (isset($_GET['searchedGrammar'])) {
        $categories = [
            'ADJ','ADV','ART','AUX','CON','LIA','NOM','ONO','PRE','PRO','VER',
        ];
        if (!in_array($_GET['searchedGrammar'], $categories)) {
            return 6;
        }
        $searchedGrammar = $_GET['searchedGrammar'];
    } else {
        $searchedGrammar = "ADJ";
    }

    $arrayWords = $manager->getRandomList($minFrequency, $maxFrequency, $nbWords);
    return [$arrayWords, $searchedGrammar];
}
