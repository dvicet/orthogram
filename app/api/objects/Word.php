<?php

class Word implements JsonSerializable {
    private $_word;
    private $_grammaticalCategory;
    private $_possibleGrammaticalCategory;

    public function __construct(array $data) {
        $this->hydrate($data);
    }

    public function hydrate(array $data) {
        foreach ($data as $key => $value) {
            $method = 'set'.ucfirst($key);

            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

    public function word() {
        return $this->_word;
    }

    public function grammaticalCategory() {
        return $this->_grammaticalCategory;
    }

    public function possibleGrammaticalCategory() {
        return $this->_possibleGrammaticalCategory;
    }

    public function setWord(string $word) {
        $this->_word = $word;
    }

    public function setGrammaticalCategory(string $grammar) {
        $this->_grammaticalCategory = $grammar;
    }

    public function setPossibleGrammaticalCategory(string $possibleGrammar) {
        $this->_possibleGrammaticalCategory = explode(",", $possibleGrammar);
    }

    public function jsonSerialize() {
        return [
            'word' => $this->word(),
            'grammaticalCategory' => $this->grammaticalCategory(),
        ];
    }
}
