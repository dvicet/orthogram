<?php

class RandomWordRequest {
    private $_minFrequency;
    private $_maxFrequency;
    private $_nbWord;

    public function __construct(
        float $minFrequency = 0,
        float $maxFrequency = MAX_FREQUENCY,
        int $nbWords = 50
    ) {
        $this->setMinFrequency($minFrequency);
        $this->setMaxFrequency($maxFrequency);
        $this->setNbWords($nbWords);
    }

    public function setMinFrequency(float $frequency) {
        if ($frequency >= 0 AND $frequency <= MAX_FREQUENCY) {
            $this->_minFrequency = $frequency;
        }
    }

    public function setMaxFrequency(float $frequency) {
        if ($frequency >= 0 AND $frequency <= MAX_FREQUENCY) {
            $this->_maxFrequency = $frequency;
        }
    }

    public function setNbWords(int $nb) {
        $this->_nbWord;
    }
}
