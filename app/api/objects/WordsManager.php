<?php

class WordsManager {
    private $_database;

    public function __construct(PDO $db) {
        $this->setDatabase($db);
    }

    public function setDatabase(PDO $db) {
        $this->_database = $db;
    }

    public function get(string $word, string $grammar) {
        $query =
            "SELECT word, grammaticalCategory FROM lexicon
            WHERE word = :word COLLATE utf8mb4_bin
            AND grammaticalCategory LIKE :grammar"; // COLLATE utf8mb4_bin -> case sensitive
        $stmt = $this->_database->prepare($query);
        $stmt->bindValue(':word', $word);
        $stmt->bindValue(':grammar', $grammar.'%'); // begin with 'grammar...'
        $stmt->execute();

        $data = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($data === false) {
            return false;
        }
        return new Word($data);
    }

    public function getRandomList(float $minFrequency, float $maxFrequency, int $nbWords) {
        $words = [];

        $query =
            "SELECT word, possibleGrammaticalCategory FROM lexicon
            WHERE bookWordFrequency >= :min AND bookWordFrequency <= :max
            ORDER BY RAND()
            LIMIT :nbWords";
        $stmt = $this->_database->prepare($query);
        $stmt->bindValue(':min', $minFrequency);
        $stmt->bindValue(':max', $maxFrequency);
        $stmt->bindValue(':nbWords', $nbWords, PDO::PARAM_INT);
        $stmt->execute();

        while($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $words[] = new Word($data);
        }

        return $words;
    }
}
