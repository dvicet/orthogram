<?php
function loadClass($class)
{
    require 'objects/'.$class.'.php';
}

spl_autoload_register('loadClass');
