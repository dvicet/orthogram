<?php
class Database{
    private $host;
    private $db_name;
    private $username;
    private $password;
    private $options;
    public $conn;
    #Print SQL Errors
    private $SQL_ERRORS;

    public function __construct() {
        $this->host = getenv("DATABASE_HOST");
        $this->db_name = getenv("DATABASE_NAME");
        $this->username = getenv("DATABASE_USER");
        $this->password = getenv("DATABASE_PASSWORD");
        if(getenv("SQL_ERRORS") === "true") {
            $SQL_ERRORS = true;
        } else {
            $SQL_ERRORS = false;
        }

        if($SQL_ERRORS) {
            $this->options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
        }
        else {
            $this->options = [];
        }
    }

    public function getConnection(){
        $this->conn = null;
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password, $this->options);
            $this->conn->exec("set names utf8mb4");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->conn;
    }
}
?>
