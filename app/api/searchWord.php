<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

require "config/autoload.php";
require "config/database.php";

const MAX_FREQUENCY = 38928.9;
const MAX_LIMIT = 1000;

$database = new Database();
$db = $database->getConnection();
$wordManager = new WordsManager($db);

if (!isset($_GET['word']) OR !isset($_GET['grammar'])) {
    $result = false;
    $message = "word and grammar must be specified";
} else {
    $word = $wordManager->get($_GET['word'], $_GET['grammar']);

    if ($word === false) {
        $result = true;
        $exists = false;
        $message = "The word ".$_GET['word']." in the grammatical category ".$_GET['grammar']." doesn't exists";
    } else {
        $result = true;
        $exists = true;
    }
}

if ($result) {
    if ($exists) {
        echo json_encode([
                "result" => $result,
                "exists" => $exists,
                "word" => $word
            ]);
    } else {
        echo json_encode([
                "result" => $result,
                "exists" => $exists,
                "message" => $message
            ]);
    }
} else {
    http_response_code(400);
    echo json_encode([
            "result" => $result,
            "message" => $message
        ]);
}
